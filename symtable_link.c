struct symtable
{
	char *pcKey;
	void *pvValue;
	struct symtable *next;
}*head,*current;

SymTable_t SymTable_new(void)
{
	if(head==NULL)
       {
       head=(struct symtable*)malloc(sizeof(struct symtable));
       head->next=NULL;
       current=head;
       assert(head!=NULL);
   }
   else
   {
   
   current->next=(SymTable_t) malloc(sizeof(struct symtable));
   current=current->next;
   current->next=NULL;
   assert(current!=NULL);
   }
   return head;
   
}
void SymTable_free (SymTable_t oSymTable)
{
	head=oSymTable;
	while(head)
	{
		current=head->next;
		free(head);
		head=current;
	}
	
}
int SymTable_getLength(SymTable_t oSymTable)
{
	int count=0;
	current=oSymTable;
	while(current)
	{
		count++;
		current=current->next;
	}
	return count;
